package plantes.plantes;

public class Declinus extends Planta {

	boolean creix = true;
	
	public Declinus() {
		
		super();
		
	}
	
	@Override
	Llavor creix() {

		if (Jardi.n % 2 == 0) {

			if (creix == true) {

				if (altura < 4) {

					altura++;

				} else {
					
					Llavor L = new Llavor(new Declinus());

					creix = false;

					System.out.println(creix);
					
					return L;

				}
			} else {
				if (altura > 0) {

					altura--;

				}else {
					
					esViva = false;
					
				}
			}

		}
		return null;

	}

	@Override
	char getChar(int nivell) {

		if (nivell > altura) {

			return ' ';
		} else {

			if (nivell != altura) {

				return ':';

			} else {

				return '*';
			}

		}
	}

}
