package plantes.plantes;

public class Llavor extends Planta{

	Planta planta;
	int creix;
	
	Llavor(Planta p){
		
		if(p instanceof Llavor) {
		
			throw new IllegalArgumentException();
			
		}else {
		
		this.creix = 0;
		
		this.planta = p;
		
		}
	}
	
	@Override
	public Planta creix() {
		

			this.creix++;
			
			System.out.println(creix);
			
			if(this.creix < 5){

				
				return null;
				
				
			}else {
				
				
				return this.planta;

			}
				
	}

	@Override
	char getChar(int nivell) {

		if (nivell == 1) {
			
			return '.';
			
		}
		
		return ' ';
	}
	
}
