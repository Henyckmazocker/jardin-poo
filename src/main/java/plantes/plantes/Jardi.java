package plantes.plantes;

public class Jardi {

	public static Planta[] jardi;

	public static int n = 0;

	public static Planta p;
	
	Jardi(int n) {

		if (n < 0) {

			jardi = new Planta[10];

		} else {
			jardi = new Planta[n];
		}

	}

	public static void temps() {

		n++;
		

		for (int i = 0; i < jardi.length; i++) {
			
			System.out.println(jardi[i]);

			if (jardi[i] == null) {
				continue;
			} else {
				
				if(!jardi[i].esViva) {
					jardi[i] = null;
					continue;
				}
				
				Planta p = jardi[i].creix();

				if (p instanceof Llavor) {
					
					boolean in = true;

					while (in) {

						try {

							plantaLlavor((Llavor) p , p.escampaLlavor(), i);

							in = false;

						} catch (ArrayIndexOutOfBoundsException e) {

							e.printStackTrace();
							continue;

						}

					}

				} else {

					if (p instanceof Planta) {

						System.out.println("si");
						
						jardi[i] = p;

					} else {
						continue;
					}

				}

			}
		}

	}

	public String toString() {

		String res = "";

		for (int j = 10; j > 0; j--) {
			for (int i = 0; i < jardi.length; i++) {
				if (jardi[i] == null) {
					res = res + " ";
				} else {
					res = res + jardi[i].getChar(j);
				}
			}
			res = res + "\n";
			if (j == 1) {
				for (int k = 0; k < jardi.length; k++) {
					res = res + "_";
				}
			}

		}

		return res;

	}

	public static boolean plantaLlavor(Planta planta, int pos, int pla) {

			if (jardi[pla+pos] == null) {
				
				jardi[pla + pos] = planta;
				return true;
				
			}
		
		return false;

	}

}
