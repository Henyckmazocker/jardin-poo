package plantes.plantes;

import java.util.Random;

public abstract class Planta 
{

	Boolean esViva;
	int altura;
	
	public Planta() {
		this.esViva = true;
		this.altura = 0;
		
	}
	
	abstract char getChar(int nivell);
	
	abstract Planta creix();
	
	public static int escampaLlavor() {
				
		Random randomnum = new Random();
		
		int n =  randomnum.nextInt(4)-2;
		
		System.out.println(n);

		
		return n;
		
	}
	
	public int getAltura() {
		return altura;
	}
	
	public boolean esViva() {
		return esViva;
	}
	
}
