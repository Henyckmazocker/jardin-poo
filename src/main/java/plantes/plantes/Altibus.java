package plantes.plantes;

public class Altibus extends Planta {
	
	public Altibus() {
		super();	
		
	}

	@Override
	Llavor creix() {

		if (altura < 10) {

			if (altura > 7) {

				Llavor L = new Llavor(new Altibus());
				
				altura++;

				return L;

			}
			
			altura++;
			
		} else {

			this.esViva = false;

			return null;
		}
		return null;

	}

	@Override
	char getChar(int nivell) {

		if (nivell > altura) {

			return ' ';
		} else {

			if (nivell != altura) {

				return '|';

			} else {

				return 'O';
			}

		}
	}

}
